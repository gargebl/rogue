TEMPLATE = app

CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES +=                 \
	src/draw.c         \
	src/engine.c       \
	src/main.c         \
	src/map.c          \
	src/os/wangblows.c \
	src/player.c

HEADERS = include/rogue.h
