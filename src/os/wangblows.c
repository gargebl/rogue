#include <conio.h>
#include <windows.h>

void gotoxy(int x, int y)
{
    COORD coord;

    coord.X = x;
    coord.Y = y;
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

    SetConsoleCursorPosition(hConsole, coord);
}

void mvaddch(int y, int x, const char ch)
{
    gotoxy(x, y);
    _putch(ch);
}

void clearScreen(void)
{
    //
}

void cursesSetup(void)
{
    //
}

void cursesCleanup(void)
{
    //
}
