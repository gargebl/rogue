#include <rogue.h>

void clearScreen(void)
{
    clear();
}

void cursesSetup(void)
{
    initscr();
    noecho();
    curs_set(0);
}

void cursesCleanup(void)
{
    endwin();
}
