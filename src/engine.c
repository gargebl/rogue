#include <rogue.h>

void gameLoop(void)
{
    int ch;

    while (1)
    {
        drawEverything();

        ch = getch();
        if (ch == 'q')
        {
            break;
        }
        handleInput(ch);
    }
}

void closeGame(void)
{
    cursesCleanup();
    free(player);
}
