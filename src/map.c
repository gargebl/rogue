#include <rogue.h>

static void markWalkableRectangle(int y0, int y1, int x0, int x1)
{
    // TODO assert "y0<y1" and "x0<x1"

    for (int y = y0; y <= y1; y++)
    {
        for (int x = x0; x <= x1; x++)
        {
            map[y][x].ch = '.';
            map[y][x].walkable = true;
        }
    }
}

static void markWalkableLine(Position p0, Position p1)
{
    int x, y, dx, dy;

    // TODO use parameterized function instead

    map[p0.y][p0.x].ch = '.';
    map[p0.y][p0.x].walkable = true;

    x = p0.x;
    y = p0.y;

    for (int i = 0; i < 100; i++)
    {
        dy = (p1.y - y);
        if (dy)
            dy /= dy * ((dy < 0) ? -1 : 1);

        dx = (p1.x - x);
        if (dx)
            dx /= dx * ((dx < 0) ? -1 : 1);

        map[y][x+dx].ch = '.';
        map[y][x+dx].walkable = true;
        map[y+dy][x+dx].ch = '.';
        map[y+dy][x+dx].walkable = true;

        y += dy;
        x += dx;
    }
}

static void generateRoom(void)
{
    int x0, x1, y0, y1;

    x1 = rand() % MAP_WIDTH;
    x1 = MAX(1, x1);
    x0 = rand() % x1;

    y1 = rand() % MAP_HEIGHT;
    y1 = MAX(1, y1);
    y0 = rand() % y1;

    markWalkableRectangle(y0, y1, x0, x1);
}

static void generateMap(void)
{
    int numrooms;

    numrooms = rand() % 5;
    numrooms = MAX(1, numrooms);

    for (int i = 0; i < numrooms; i++)
    {
        generateRoom();
    }

    markWalkableLine(
        (Position){(rand() % MAP_HEIGHT), (rand() % MAP_WIDTH)},
        (Position){(rand() % MAP_HEIGHT), (rand() % MAP_WIDTH)}
        );
}

static Position findStartPosition(void)
{
    // TODO make this smarter and cooler

    for (int y = 0; y < MAP_HEIGHT; y++)
    {
        for (int x = 0; x < MAP_WIDTH; x++)
        {
            if (map[y][x].walkable) return (Position){y, x};
        }
    }

    return (Position){0, 0};
}

Tile** createMapTiles(void)
{
    Tile** tiles = calloc(MAP_HEIGHT, sizeof(Tile*));

    for (int y = 0; y < MAP_HEIGHT; y++)
    {
        tiles[y] = calloc(MAP_WIDTH, sizeof(Tile));

        for (int x = 0; x < MAP_WIDTH; x++)
        {
            tiles[y][x].ch = '#';
            tiles[y][x].walkable = false;
        }
    }

    return tiles;
}

Position setupMap(void)
{
    generateMap();

    return findStartPosition();
}

void freeMap(void)
{
    for (int y = 0; y < MAP_HEIGHT; y++)
    {
        free(map[y]);
    }

    free(map);
}
