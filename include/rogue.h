#ifndef ROGUE_H
#define ROGUE_H

#ifdef _WIN32
    #include <conio.h>
#else
    #include <ncurses.h>
#endif // _WIN32

#include <stdbool.h>
#include <stdlib.h>

#define  MAX(A, B)  ((A) > (B) ? (A) : (B))

typedef struct
{
    int y;
    int x;
} Position;

typedef struct
{
    char ch;
    bool walkable;
} Tile;

typedef struct
{
    Position pos;
    char ch;
} Entity;

// draw.c functions
void drawMap(void);
void drawEntity(Entity* entity);
void drawEverything(void);

// engine.c functions
void gameLoop(void);
void closeGame(void);

// map.c functions
Tile** createMapTiles(void);
Position setupMap(void);
void freeMap(void);

// player.c functions
Entity* createPlayer(Position start_pos);
void handleInput(int input);
void movePlayer(Position newPos);

// os-specific functions
void clearScreen(void);
void cursesSetup(void);
void cursesCleanup(void);

// externs
extern const int MAP_HEIGHT;
extern const int MAP_WIDTH;
extern Entity* player;
extern Tile** map;

#endif
