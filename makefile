CC = gcc
CFLAGS = -lncurses -I./include/ -Wall -Wextra
SOURCES = ./src/*.c

all: rogue_linux

# TODO not entirely sure this is needed?
rogue_linux:
	@make rogue SOURCES="$(SOURCES) ./src/os/lincux.c"

rogue: $(SOURCES)
	$(CC) $(SOURCES) $(CFLAGS) -o rogue

run: rogue
	./rogue

clean:
	rm -f rogue
